/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author gjgresham
 */
public class IRImage {
    
    int revision = 0;
    int imageWidth, imageHeight;
    boolean usesInlinePalette;
    boolean color1Transparency;
    boolean color2Transparency;
    boolean twobytewidth=false;
    boolean twobyteheight=false;
    int[][] data;
    FossilSprite imageEditor;
    
    public IRImage(FossilSprite imageEditor, int revision, int imageWidth, int imageHeight){
        
        this.revision = revision;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.imageEditor = imageEditor;
        if(imageWidth>256){
            twobytewidth=true;
        }
        if(imageHeight>256){
            twobyteheight=true;
        }
        
        System.out.println("Image Width: " + imageWidth + " Image Height: " + imageHeight);
        data = new int[imageWidth][imageHeight];
        
    }
    
    public void setData(int[][] become){
        data = new int[become[0].length][become.length];
        for(int j=0; j<data.length; j++){
            for(int i=0; i<data[0].length; i++){
                data[i][j] = become[i][j];
            }
        }
    }
    
    public static IRImage loadOBG(FossilSprite imageEditor, File file){
        /*OBGFile temp = new OBGFile(file);
        IRImage image = new IRImage(imageEditor, temp.revision, temp.imageWidth, temp.imageHeight);
        image.revision = temp.revision;
        image.imageWidth = temp.imageWidth;
        image.imageHeight = temp.imageHeight;
        
        for(int j=0; j<image.imageHeight; j++){
            for(int i=0; i<image.imageWidth; i++){
                if(temp.imageData[i][j]){
                    image.data[i][j] = 1;
                }else{
                    image.data[i][j] = 0;
                }
            }
        }
        return image;*/
        return loadImage(imageEditor, file, 1);
    }
    
    public static IRImage loadTBG(FossilSprite imageEditor, File file){
        return loadImage(imageEditor, file, 2);
    }
    
    public static IRImage loadFBG(FossilSprite imageEditor, File file){
        return loadImage(imageEditor, file, 4);
    }
    
    public static IRImage loadEBG(FossilSprite imageEditor, File file){
        return loadImage(imageEditor, file, 8);
    }
    
    public static IRImage loadSBG(FossilSprite imageEditor, File file){
        return loadImage(imageEditor, file, 16);
    }
    
    private static IRImage loadImage(FossilSprite imageEditor, File file, int numBits){
        byte[]imageB = new byte[(int)file.length()];
        
        try {
            FileInputStream in = new FileInputStream(file);
            in.read(imageB);
            in.close();
        }catch(Exception e){
            
        }
        //OBGFile temp = new OBGFile(file);
        
        int rev = imageB[0];
        
        boolean usesInlinePalette=false;
        boolean color1Transparency=false;
        boolean color2Transparency=false;
        
        if(rev==0){
            
            if(DataTools.convertByte(imageB[1]) >> 7 == 1){
                usesInlinePalette=true;
                if((DataTools.convertByte(imageB[1]) << 1 >> 7 & 1) == 1 && (DataTools.convertByte(imageB[1]) << 2 >> 7 & 1) == 1){
                    color1Transparency=true;
                    color2Transparency=true;
                    System.out.println("Both colors are transparent, and nothing will be drawn.");
                }else if((DataTools.convertByte(imageB[1]) << 1 >> 7 & 1) == 1){
                    color1Transparency=true;
                }else if((DataTools.convertByte(imageB[1]) << 2 >> 7 & 1) == 1){
                    color2Transparency=true;
                }else{
                    //placeholder
                }
            }
                
            int fileStartOffset = 2;
            if(usesInlinePalette){
                if(!color1Transparency && !color2Transparency){
                    fileStartOffset += 8;
                }else if(color1Transparency || color2Transparency){
                    fileStartOffset += 4;
                }
            }
            
            IRImage image;
            int tempWidth, tempHeight;
            
           // System.out.println("TEST 2: " + (DataTools.convertByte(imageB[1]) << 2 >> 7) + " " + (DataTools.convertByte(imageB[1]) << 3 >> 7) + " " + (DataTools.convertByte(imageB[1]) << 4 >> 7 & 1));
            if((DataTools.convertByte(imageB[1]) << 3 >> 7 & 1) == 1){
                tempWidth=(imageB[fileStartOffset] << 8) + imageB[fileStartOffset+1];
                System.out.println("TEST: " + imageB[fileStartOffset] + "   " + imageB[fileStartOffset+1] + "   " + ((imageB[fileStartOffset] << 8) + imageB[fileStartOffset+1]));
                fileStartOffset+=2;
            }else{
                tempWidth=DataTools.convertByte(imageB[fileStartOffset]);
                fileStartOffset++;
            }
            
            if((DataTools.convertByte(imageB[1]) << 4 >> 7 & 1) == 1){
                tempHeight=(imageB[fileStartOffset] << 8) + imageB[fileStartOffset+1];
                System.out.println("TEST: " + imageB[fileStartOffset] + "   " + imageB[fileStartOffset+1] + "   " + ((imageB[fileStartOffset] << 8) + imageB[fileStartOffset+1]));
                fileStartOffset+=2;
            }else{
                tempHeight=DataTools.convertByte(imageB[fileStartOffset]);
                //System.out.println("TEST: " + imageB[fileStartOffset] + "   " + imageB[fileStartOffset+1] + "   " + ((imageB[fileStartOffset] << 8) + imageB[fileStartOffset+1]));
                fileStartOffset++;
            }
            
            image = new IRImage(imageEditor, rev, tempWidth, tempHeight);
            
            //image.revision = temp.revision;
            //image.imageWidth = temp.imageWidth;
            //image.imageHeight = temp.imageHeight;
        
            int offset=0;
        
            int bitmask=0;
            for(int i=0; i<numBits; i++){
                bitmask = bitmask << 1;
                bitmask++;
            }
            
            System.out.println("BITMASK: " + bitmask);
        
            int tempPixel = 0;
        
            if(numBits<=8){
                for(int j=0; j<image.imageHeight; j++){
                    for(int i=0; i<image.imageWidth; i++){
                        tempPixel = 0;
                        //System.out.println("Offset: " + offset%8);
                        //System.out.println("Bit: " + ((imageBytes[fileStartOffset+2+(offset/8)] >> offset%8) & 1));
                        for(int k=0; k<numBits; k++){
                            tempPixel = tempPixel << 1;
                            tempPixel += ((imageB[fileStartOffset+(offset/8)] >> (7 - offset%8) ) & 1);
                            //System.out.println("XPos: " + i + " YPos: " + j + " iteration: " + k + " offset: " + offset%8 + " tempPixel: " + tempPixel);
                            offset++;
                        }
                    
                        image.data[i][j] = tempPixel & bitmask;
                    }
                }
            }else{
                for(int j=0; j<image.imageHeight; j++){
                    for(int i=0; i<image.imageWidth; i++){
                        tempPixel = imageB[fileStartOffset+offset];
                        //System.out.println("WIP Color at " + i + ", " + j + ": " + (tempPixel & 0xFFFF));
                        tempPixel = tempPixel << 8;
                        //System.out.println("WIP Color at " + i + ", " + j + ": " + (tempPixel & 0xFFFF));
                        tempPixel = tempPixel | DataTools.convertByte(imageB[fileStartOffset+(offset+1)]);
                        //System.out.println("Second byte: " + DataTools.convertByte(imageB[fileStartOffset+(offset+1)]));
                        //System.out.println("WIP Color at " + i + ", " + j + ": " + (tempPixel & 0xFFFF));
                        offset+=2;
                        
                        /*if(tempPixel>=imageEditor.palette.color.length){
                            System.out.println("COLOR LARGER THAN PALETTE!");
                        }else{
                            System.out.println("Color at " + i + ", " + j + ": " + (tempPixel & 0xFFFF) + " " + fileStartOffset+offset);
                        }*/
                    
                        image.data[i][j] = tempPixel & bitmask;
                    }
                }
            }
            
            return image;
        }
        return null;
    }
    
    public static IRImage importImage(FossilSprite imageEditor, File file){
        IRImage image = new IRImage(imageEditor, 0, 0, 0);
        
        image.importPNG(file);
        
        return image;
    }
    
    public void save(File file, FossilSprite imageEditor, boolean reorganizePalette){
        
        ArrayList<Integer> palTemp = new ArrayList<>();
        boolean palExists=false;
        
        //System.out.println("Pal Size " + palTemp.size());
        
        int[] tempP;
        int[][]temp = new int[imageWidth][imageHeight];
        
        if(reorganizePalette){
            for (int j = 0; j < imageHeight; j++) {
            for (int i = 0; i < imageWidth; i++) {
                palExists=false;
                for(int k=0; k<palTemp.size(); k++){
                    if(palTemp.get(k)==data[i][j]){
                        palExists=true;
                    }
                }
                if(!palExists){
                    palTemp.add(data[i][j]);
                }
            }
        }
            
            tempP = new int[palTemp.size()];
        for(int i=0; i<tempP.length; i++){
            tempP[i]=palTemp.get(i);
        }
        
        Arrays.sort(tempP);
        
        //temp = new int[imageWidth][imageHeight];
        
         for (int j = 0; j < imageHeight; j++) {
            for (int i = 0; i < imageWidth; i++) {
                for(int k=0; k<tempP.length; k++){
                    if(data[i][j] == tempP[k]){
                        temp[i][j] = k;
                    }
                }
            }
        }
         
        System.out.println("Counted Colors");
        }else{
            tempP = new int[imageEditor.palette.color.length];
            for(int i=0; i<tempP.length; i++){
                tempP[i] = i;
            }
            for (int j = 0; j < imageHeight; j++) {
                for (int i = 0; i < imageWidth; i++) {
                    temp[i][j] = data[i][j];    
                }
            }
        }
        
        if(tempP.length<=2){
            saveImage(tempP, temp, file, 1, false);
        }else if(tempP.length<=4){
            saveImage(tempP, temp, file, 2, false);
        }else if(tempP.length<=16){
            saveImage(tempP, temp, file, 4, false);
        }else if(tempP.length<=256){
            saveImage(tempP, temp, file, 8, false);
        }else if(tempP.length<=65525){
            saveImage(tempP, temp, file, 16, false);
        }
        
    }
    
    public void saveOBG(int[]palette, int[][] image, File file){
        try{
        DataOutputStream os = new DataOutputStream(new FileOutputStream(file + ".obg"));
        //os.writeInt(i);
        os.writeByte(0);
        os.writeByte(0);
        if(imageWidth==256){
            os.writeByte(0);
        }else{
            os.writeByte(imageWidth);
        }
        
        if(imageHeight==256){
            os.writeByte(0);
        }else{
            os.writeByte(imageHeight);
        }
        byte b = 0;
        int offset=0;
        for(int j=0; j<imageHeight; j++){
            for(int i=0; i<imageWidth; i++){
                b = (byte)(b << 1);
                //System.out.println("Unshifted: " + b);
                if(image[i][j]==1){
                    b = (byte)(b | 1);
                    //System.out.println("Shifted: " + b);
                }
                
                if(offset==7){
                    offset=0;
                    os.writeByte(b);
                    //System.out.println("WROTE BYTE");
                    b=0;
                }else{
                    offset++;
                }
            }
        }
        
        if(offset!=0){
            while(offset<8){
                b = (byte)(b << 1);
                offset++;
            }
            os.writeByte(b);
        }
        os.close();
        
        os = new DataOutputStream(new FileOutputStream(file + ".spal"));
        
        char numColors = (char)(palette.length & 0xFFFF);
        
        os.writeChar(numColors);
        
        for(int i=0; i<numColors; i++){
            os.writeByte(imageEditor.palette.color[palette[i]].getRed());
            os.writeByte(imageEditor.palette.color[palette[i]].getGreen());
            os.writeByte(imageEditor.palette.color[palette[i]].getBlue());
            os.writeByte(imageEditor.palette.color[palette[i]].getAlpha());
        }
        
        os.close();
        }catch(Exception e){
            
        }
    }
    
    public void saveTBG(int[]palette, int[][] image, File file){
        try{
        DataOutputStream os = new DataOutputStream(new FileOutputStream(file + ".tbg"));
        //os.writeInt(i);
        os.writeByte(0);
        os.writeByte(0);
        if(imageWidth==256){
            os.writeByte(0);
        }else{
            os.writeByte(imageWidth);
        }
        
        if(imageHeight==256){
            os.writeByte(0);
        }else{
            os.writeByte(imageHeight);
        }
        byte b = 0;
        int offset=0;
        
        for(int j=0; j<imageHeight; j++){
            for(int i=0; i<imageWidth; i++){
                b = (byte)(b << 2);
                //System.out.println("Unshifted: " + b);
                //if(image[i][j]==1){
                b = (byte)(b | (image[i][j] & 0b00000011));
                    //System.out.println("Shifted: " + b);
                //}
                
                if(offset==3){
                    offset=0;
                    os.writeByte(b);
                    //System.out.println("WROTE BYTE");
                    b=0;
                }else{
                    offset++;
                }
            }
        }
        
        
        if(offset!=0){
            while(offset<4){
                b = (byte)(b << 2);
                offset++;
            }
            os.writeByte(b);
        }
        os.close();
        
        os = new DataOutputStream(new FileOutputStream(file + ".spal"));
        
        char numColors = (char)(palette.length & 0xFFFF);
        
        os.writeChar(numColors);
        
        for(int i=0; i<numColors; i++){
            os.writeByte(imageEditor.palette.color[palette[i]].getRed());
            os.writeByte(imageEditor.palette.color[palette[i]].getGreen());
            os.writeByte(imageEditor.palette.color[palette[i]].getBlue());
            os.writeByte(imageEditor.palette.color[palette[i]].getAlpha());
        }
        
        os.close();
        }catch(Exception e){
            
        }
    }
    
    public void saveImage(int[]palette, int[][] image, File file, int numBits, boolean isVariable){
        try{
        DataOutputStream os = null;
        
        switch(numBits){
            
            case 1:
                os = new DataOutputStream(new FileOutputStream(file.getAbsolutePath().replaceFirst("[.][^.]+$", "") + ".obg"));
                System.out.println("OUTPUT FILE: " + file.getAbsolutePath().replaceFirst("[.][^.]+$", "") + ".obg");
                break;
                
            case 2:
                os = new DataOutputStream(new FileOutputStream(file.getAbsolutePath().replaceFirst("[.][^.]+$", "") + ".tbg"));
                break;
                
            case 4:
                os = new DataOutputStream(new FileOutputStream(file.getAbsolutePath().replaceFirst("[.][^.]+$", "") + ".fbg"));
                break;
                
            case 8:
                os = new DataOutputStream(new FileOutputStream(file.getAbsolutePath().replaceFirst("[.][^.]+$", "") + ".ebg"));
                break;
                
            case 16:
                os = new DataOutputStream(new FileOutputStream(file.getAbsolutePath().replaceFirst("[.][^.]+$", "") + ".sbg"));
                break;
            
        }
        //os.writeInt(i);
        os.writeByte(0);
        byte b = 0;
        if(usesInlinePalette){
            b = (byte)(b | 1);
        }
        b = (byte)(b << 1);
        if(color1Transparency){
            b = (byte)(b | 1);
        }
        b = (byte)(b << 1);
        if(color2Transparency){
            b = (byte)(b | 1);
        }
        b = (byte)(b << 1);
        if(twobytewidth){
            b = (byte)(b | 1);
        }
        b = (byte)(b << 1);
        if(twobyteheight){
            b = (byte)(b | 1);
        }
        b = (byte)(b << 3);
        os.writeByte(b);
        
        if(twobytewidth){
            if(imageWidth==65535){
                os.writeByte(0);
                os.writeByte(0);
            }else{
                os.writeByte(imageWidth >> 8);
                os.writeByte(imageWidth & 0xFF);
            }
        }else{
            if(imageWidth==256){
                os.writeByte(0);
            }else{
                os.writeByte(imageWidth);
            }
        }
        
        if(twobyteheight){
            if(imageHeight==65535){
                os.writeByte(0);
                os.writeByte(0);
            }else{
                os.writeByte(imageHeight >> 8);
                os.writeByte(imageHeight & 0xFF);
            }
        }else{
            if(imageHeight==256){
                os.writeByte(0);
            }else{
                os.writeByte(imageHeight);
            }
        }
        
        b = 0;
        int offset=0;
        
        int bitmask=0;
        for(int i=0; i<numBits; i++){
            bitmask = bitmask << 1;
            bitmask++;
        }
        
        if(numBits<=8){
            for(int j=0; j<imageHeight; j++){
                for(int i=0; i<imageWidth; i++){
                    b = (byte)(b << numBits);
                    //System.out.println("Unshifted: " + b);
                    //if(image[i][j]==1){
                    b = (byte)(b | (image[i][j] & bitmask));
                    //System.out.println("Shifted: " + b);
                    //}
                
                    if(offset==(8/numBits)-1){
                        offset=0;
                        os.writeByte(b);
                        //System.out.println("WROTE BYTE");
                        b=0;
                    }else{
                        offset++;
                    }
                }
            }
        
            if(offset!=0){
                while(offset<(8/numBits)-1){
                    b = (byte)(b << numBits);
                    offset++;
                }
                os.writeByte(b);
            }
        }else{
            char s = 0;
            for(int j=0; j<imageHeight; j++){
                for(int i=0; i<imageWidth; i++){
                    s = 0;
                    //System.out.println("Unshifted: " + b);
                    //if(image[i][j]==1){
                    s = (char)(image[i][j] & bitmask);
                    os.writeByte(s >> 8);
                    os.writeByte(s & 0xFF);
                    //System.out.println("Shifted: " + b);
                    //}
                }
            }
        }
        
        
        
        os.close();
        
        os = new DataOutputStream(new FileOutputStream(file + ".spal"));
        
        char numColors = (char)(palette.length & 0xFFFF);
        
        System.out.println("Palette Size: " + palette.length + "   numColors: " + (int)(numColors));
        
        os.writeChar(numColors);
        
        for(int i=0; i<numColors; i++){
            os.writeByte(imageEditor.palette.color[palette[i]].getRed());
            os.writeByte(imageEditor.palette.color[palette[i]].getGreen());
            os.writeByte(imageEditor.palette.color[palette[i]].getBlue());
            os.writeByte(imageEditor.palette.color[palette[i]].getAlpha());
        }
        
        os.close();
        }catch(Exception e){
            
        }
    }
    
    public void resize(int width, int height, int backgroundColor){
        int[][] temp = new int[width][height];
        
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                if(i<data.length && j<data[0].length){
                    temp[i][j] = data[i][j];
                }else{
                    temp[i][j] = backgroundColor;
                }
            }
        }
        
        data = temp;
        imageWidth = width;
        imageHeight = height;
    }
    
    public void importPNG(File file){
        try {
            BufferedImage temp = ImageIO.read(file);
            imageEditor.debug = temp;
            /*if(temp.getWidth()<=255 && temp.getHeight()<=255){
                imageWidth = temp.getWidth();
                imageHeight = temp.getHeight();
            }else if((temp.getWidth()<=255 && temp.getHeight()==256) || (temp.getHeight()<=255 && temp.getWidth()==256) || (temp.getHeight()==256 && temp.getWidth()==256)){
                //if(temp.getWidth()==256){
                //   imageWidth = 0;
                //}else{
                   imageWidth = temp.getWidth(); 
                //}
                
                //if(temp.getHeight()==256){
                //    imageHeight = 0;
                //}else{
                    imageHeight = temp.getHeight();
                //}
            }else{
                
            }*/
            
            imageWidth = temp.getWidth();
            imageHeight = temp.getHeight();
            
            if(imageWidth>256){
                twobytewidth=true;
            }
            if(imageHeight>256){
                twobyteheight=true;
            }
            
            //int bitDepth = 0;
            //int testDepth = temp.getRGB(0, 0);
            //Integer.bitCount(testDepth);
            
            /*while (testDepth > 0){ 
                bitDepth ++; 
                testDepth >>= 1; 
            }*/
            
            //System.out.println("Bit Depth:" + Integer.bitCount(testDepth));
            
            //if(bitDepth==24){
                importPNG24B(temp);
            //}
            
            
        } catch (IOException ex) {
            Logger.getLogger(OBGFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void importPNG24B(BufferedImage temp){
        
        //ArrayList<Integer> tempListR = new ArrayList<>();
        //ArrayList<Integer> tempListG = new ArrayList<>();
        //ArrayList<Integer> tempListB = new ArrayList<>();
        //ArrayList<Integer> tempListA = new ArrayList<>();
        ArrayList<Integer> tempListQ = new ArrayList<>();
        ArrayList<Color> tempListColors = new ArrayList<>();
        
        boolean colorExists=false;
        
        //check the image's colors and automatically generate a palette
        
        int tempWidth = imageWidth, tempHeight=imageHeight;
        
        if(imageWidth == 0){
            tempWidth = 256;
        }
        
        if(imageHeight == 0){
            tempHeight = 256;
        }
        
        Color tempColor;
        
        for(int j=0; j<tempHeight; j++){
                for(int i=0; i<tempWidth; i++){
                    colorExists=false;
                    tempColor = new Color(temp.getRGB(i, j), true);
                    if(tempColor.getAlpha()==0){
                        
                        tempColor = new Color(0, 0, 0, 0);
                    }
                    //int tempR = temp.getRGB(i, j) >> 16 & 0xFF;
                    //int tempG = temp.getRGB(i, j) >> 8 & 0xFF;
                    //int tempB = temp.getRGB(i, j) & 0xFF;
                    for(int k=0; k<tempListQ.size(); k++){
                        //if(tempR==tempListR.get(k) && tempG==tempListG.get(k) && tempB==tempListB.get(k)){
                        if(tempColor.getRGB()==tempListColors.get(k).getRGB()){
                            tempListQ.set(k, tempListQ.get(k)+1);
                            colorExists=true;
                        }
                    }
                if(!colorExists){
                    //tempListR.add(tempR);
                    //tempListG.add(tempG);
                    //tempListB.add(tempB);
                    tempListColors.add(tempColor);
                    tempListQ.add(1);
                    //System.out.println("ADDED!");
                   // System.out.println(tempColor);
                }
                //System.out.println("R: " + tempR + "G: " + tempG + "B: " + tempB);
                
            }
        }
        
        //int highest = 0, secondHighest = -1;
        
        
        int[][] tempListArray = new int[tempListQ.size()][2];
        
        for(int i=0; i<tempListQ.size(); i++){
            tempListArray[i][0] = tempListQ.get(i);
            tempListArray[i][1] = i;
        }
        
        Arrays.sort(tempListArray, (a, b) -> Double.compare(a[0], b[0]));
        
        /*for(int i=0; i<tempListQ.size(); i++){
            if(tempListQ.get(i)>=tempListQ.get(highest)){
                if(secondHighest!=-1){
                    secondHighest = highest;
                }
                
                highest=i;
            }else{
                if(secondHighest==-1){
                    secondHighest=i;
                }
            }
        }*/
        
        
        
        Color[] palette;
        
        //if(tempListQ.size()>256){
        //    palette = new Color[256];
        //}else{
            palette = new Color[tempListQ.size()];
        //}
        
        //palette[0] = tempListColors.get(tempListArray[tempListQ.size()-1][1]);
        //palette[1] = tempListColors.get(tempListArray[tempListQ.size()-2][1]);
        
        for(int i=palette.length-1; i>=0; i--){
            palette[Math.abs(i-(palette.length-1))] = tempListColors.get(tempListArray[i][1]);
        }
        
        //System.out.println("Highest: " + tempListColors.get(tempListArray[tempListQ.size()-1][1]) + " Second Highest: " + tempListColors.get(tempListArray[tempListQ.size()-2][1]) + " tempListColors.size(): " + tempListColors.size());
        
        //palette[0] = tempListColors.get(highest);
        //palette[1] = tempListColors.get(secondHighest);
        
        //usesInlinePalette=true;
        
        
        //System.out.println("numColors: " + tempListQ.size());
        
        //System.out.println("TR: " + tempListR.get(highest) + " TG: " + tempListG.get(highest) + " TB: " + tempListB.get(highest));
        //System.out.println("TR: " + tempListR.get(secondHighest) + " TG: " + tempListG.get(secondHighest) + " TB: " + tempListB.get(secondHighest));
        
        data = new int[tempWidth][tempHeight];
        
        int distance = 0, distanceID = 0, tempDistance;
        
        int tempR = 0;
        int tempG = 0;
        int tempB = 0;
        int tempA = 0;
        //Color tempColor;
        
        int transparentID = -1;
            
        for(int j=0; j<tempHeight; j++){
            for(int i=0; i<tempWidth; i++){
                distance=Integer.MAX_VALUE;
                distanceID=0;
                tempColor = new Color(temp.getRGB(i, j), true);
                tempR = tempColor.getRed();
                tempG = tempColor.getGreen();
                tempB = tempColor.getBlue();
                tempA = tempColor.getAlpha();
                System.out.println("Alpha at " + i + ", " + j + ": " + tempA);
                
                //distanceTo0 = Math.abs(tempR - palette[0].getRed()) + Math.abs(tempG - palette[0].getGreen()) + Math.abs(tempB - palette[0].getBlue());
               // distanceTo1 = Math.abs(tempR - palette[1].getRed()) + Math.abs(tempG - palette[1].getGreen()) + Math.abs(tempB - palette[1].getBlue());
                
                //System.out.println(distanceTo0 + " " + distanceTo1);
                
                //imageData[i][j] = distanceTo0 >= distanceTo1;
                
                if(tempA == 0){
                    if(transparentID==-1){
                        for(int k=0; k<palette.length; k++){
                            if(palette[k].getAlpha()==0){
                                transparentID=k;
                            }
                        }
                    }
                    //System.out.println("PIXEL HAS TRANSPARENCY!");
                    distanceID = transparentID;
                }else{
                    for(int k=0; k<palette.length; k++){
                    tempDistance = Math.abs(tempR - palette[k].getRed()) + Math.abs(tempG - palette[k].getGreen()) + Math.abs(tempB - palette[k].getBlue()) + Math.abs(tempA - palette[k].getAlpha());
                    if(tempDistance < distance){
                        distance = tempDistance;
                        distanceID = k;
                    }
                }
                }
                //System.out.println(distanceID);
                data[i][j] = distanceID;
            }
        }
        
        imageEditor.palette.color = palette;
        imageEditor.paletteBar.resize();
        
        
    }
    
    public boolean canRemoveColor(){
        for(int j=0; j<imageHeight; j++){
            for(int i=0; i<imageWidth; i++){
                if(data[i][j] == imageEditor.palette.color.length-1){
                    return false;
                }
            }
        }
        return true;
    }
    
}
