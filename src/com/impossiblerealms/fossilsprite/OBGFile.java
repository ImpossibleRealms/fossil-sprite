/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author gjgresham
 */
public class OBGFile {
    
    int revision = 0;
    boolean usesInlinePalette;
    boolean color1Transparency;
    boolean color2Transparency;
    int imageWidth = 0;
    int imageHeight = 0;
    Color[] palette;
    public boolean[][] imageData;
    
    public OBGFile(int revision, int width, int height){
        this.revision = revision;
        usesInlinePalette = false;
        color1Transparency = false;
        color2Transparency = false;
        
        this.imageWidth = width;
        this.imageHeight = height;
        
        imageData = new boolean[width][height];
        
        for(int j=0; j<imageHeight; j++){
            for(int i=0; i<imageWidth; i++){
                imageData[i][j]=false;
            }
        }
    }
    
    public OBGFile(File file){
        
        if(file.toString().endsWith("png")){
            importPNG(file);
        }else/* if(file.toString().endsWith("obg"))*/{
            load(file);
        }
    }
    
    public void resize(int width, int height){
        boolean[][] temp = new boolean[width][height];
        
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                if(i<imageData.length && j<imageData[0].length){
                    temp[i][j] = imageData[i][j];
                }else{
                    temp[i][j] = false;
                }
            }
        }
        
        imageData = temp;
        imageWidth = width;
        imageHeight = height;
        
    }
    
    public void save(File file){
        try{
        DataOutputStream os = new DataOutputStream(new FileOutputStream(file + ".obg"));
        //os.writeInt(i);
        os.writeByte(0);
        os.writeByte(0);
        if(imageWidth==256){
            os.writeByte(0);
        }else{
            os.writeByte(imageWidth);
        }
        
        if(imageHeight==256){
            os.writeByte(0);
        }else{
            os.writeByte(imageHeight);
        }
        byte b = 0;
        int offset=0;
        for(int j=0; j<imageHeight; j++){
            for(int i=0; i<imageWidth; i++){
                b = (byte)(b << 1);
                System.out.println("Unshifted: " + b);
                if(imageData[i][j]){
                    b = (byte)(b | 1);
                    System.out.println("Shifted: " + b);
                }
                
                if(offset==7){
                    offset=0;
                    os.writeByte(b);
                    System.out.println("WROTE BYTE");
                    b=0;
                }else{
                    offset++;
                }
            }
        }
        
        if(offset!=0){
            while(offset<8){
                b = (byte)(b << 1);
                offset++;
            }
            os.writeByte(b);
        }
        os.close();
        }catch(Exception e){
            
        }
    }
    
    public void load(File file){
        byte[]imageBytes = new byte[(int)file.length()];
        try {
            FileInputStream in = new FileInputStream(file);
            in.read(imageBytes);
            in.close();
            
            int fileStartOffset=2;
            
            if(DataTools.convertByte(imageBytes[0])==0){
                if(DataTools.convertByte(imageBytes[1]) >> 7 == 1){
                    usesInlinePalette=true;
                    if(DataTools.convertByte(imageBytes[1]) << 1 >> 7 == 1 && DataTools.convertByte(imageBytes[1]) << 2 >> 7 == 1){
                        color1Transparency=true;
                        color2Transparency=true;
                        System.out.println("Both colors are transparent, and nothing will be drawn.");
                    }else if(DataTools.convertByte(imageBytes[1]) << 1 >> 7 == 1){
                        color1Transparency=true;
                    }else if(DataTools.convertByte(imageBytes[1]) << 2 >> 7 == 1){
                        color2Transparency=true;
                    }else{
                        //placeholder
                    }
                }
                
                if(usesInlinePalette){
                    palette = new Color[2];
                    if(!color1Transparency){
                        palette[0]=new Color(DataTools.convertByte(imageBytes[fileStartOffset]), DataTools.convertByte(imageBytes[fileStartOffset+1]), DataTools.convertByte(imageBytes[fileStartOffset+2]), DataTools.convertByte(imageBytes[fileStartOffset+3]));
                        fileStartOffset += 4;
                    }else{
                        palette[0] = new Color(0, 0, 0, 0);
                    }
                    if(!color2Transparency){
                        palette[1]=new Color(DataTools.convertByte(imageBytes[fileStartOffset]), DataTools.convertByte(imageBytes[fileStartOffset+1]), DataTools.convertByte(imageBytes[fileStartOffset+2]), DataTools.convertByte(imageBytes[fileStartOffset+3]));
                        fileStartOffset += 4;
                    }else{
                        palette[1] = new Color(0, 0, 0, 0);
                    }
                }else{
                    palette = new Color[2];
                    palette[0] = new Color(0, 0, 0, 255);
                    palette[1] = new Color(255, 255, 255, 255);
                }
                System.out.println("Width: " + DataTools.convertByte(imageBytes[fileStartOffset]));
                System.out.println("Height: " + DataTools.convertByte(imageBytes[fileStartOffset+1]));
                imageWidth = DataTools.convertByte(imageBytes[fileStartOffset]);
                imageHeight = DataTools.convertByte(imageBytes[fileStartOffset+1]);
                
                if(imageWidth==0){
                    imageWidth=256;
                }
                
                if(imageHeight==0){
                    imageHeight=256;
                }
            
                imageData = new boolean[imageWidth][imageHeight];
            
                int offset=0;
            
                for(int j=0; j<imageHeight; j++){
                    for(int i=0; i<imageWidth; i++){
                        System.out.println("Offset: " + offset%8);
                        System.out.println("Bit: " + ((imageBytes[fileStartOffset+2+(offset/8)] >> offset%8) & 1));
                        if(((imageBytes[fileStartOffset+2+(offset/8)] >> 7-(offset%8)) & 1) == 0){
                            imageData[i][j] = false;
                        }else{
                            imageData[i][j] = true;
                        }
                        offset++;
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FossilSprite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FossilSprite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void importPNG(File file){
        try {
            BufferedImage temp = ImageIO.read(file);
            if(temp.getWidth()<=255 && temp.getHeight()<=255){
                imageWidth = temp.getWidth();
                imageHeight = temp.getHeight();
            }else if((temp.getWidth()<=255 && temp.getHeight()==256) || (temp.getHeight()<=255 && temp.getWidth()==256) || (temp.getHeight()==256 && temp.getWidth()==256)){
                //if(temp.getWidth()==256){
                //   imageWidth = 0;
                //}else{
                   imageWidth = temp.getWidth(); 
                //}
                
                //if(temp.getHeight()==256){
                //    imageHeight = 0;
                //}else{
                    imageHeight = temp.getHeight();
                //}
            }else{
                
            }
            
            
            
            int bitDepth = 0;
            int testDepth = temp.getRGB(0, 0);
            Integer.bitCount(testDepth);
            
            /*while (testDepth > 0){ 
                bitDepth ++; 
                testDepth >>= 1; 
            }*/
            
            System.out.println("Bit Depth:" + Integer.bitCount(testDepth));
            
            //if(bitDepth==24){
                importPNG24B(temp);
            //}
            
            
        } catch (IOException ex) {
            Logger.getLogger(OBGFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void importPNG24B(BufferedImage temp){
        
        //ArrayList<Integer> tempListR = new ArrayList<>();
        //ArrayList<Integer> tempListG = new ArrayList<>();
        //ArrayList<Integer> tempListB = new ArrayList<>();
        //ArrayList<Integer> tempListA = new ArrayList<>();
        ArrayList<Integer> tempListQ = new ArrayList<>();
        ArrayList<Color> tempListColors = new ArrayList<>();
        
        boolean colorExists=false;
        
        //check the image's colors and automatically generate a palette
        
        int tempWidth = imageWidth, tempHeight=imageHeight;
        
        if(imageWidth == 0){
            tempWidth = 256;
        }
        
        if(imageHeight == 0){
            tempHeight = 256;
        }
        
        for(int j=0; j<tempHeight; j++){
                for(int i=0; i<tempWidth; i++){
                    colorExists=false;
                    Color tempColor = new Color(temp.getRGB(i, j), true);
                    //int tempR = temp.getRGB(i, j) >> 16 & 0xFF;
                    //int tempG = temp.getRGB(i, j) >> 8 & 0xFF;
                    //int tempB = temp.getRGB(i, j) & 0xFF;
                    for(int k=0; k<tempListQ.size(); k++){
                        //if(tempR==tempListR.get(k) && tempG==tempListG.get(k) && tempB==tempListB.get(k)){
                        if(tempColor.getRGB()==tempListColors.get(k).getRGB()){
                            tempListQ.set(k, tempListQ.get(k)+1);
                            colorExists=true;
                        }
                    }
                if(!colorExists){
                    //tempListR.add(tempR);
                    //tempListG.add(tempG);
                    //tempListB.add(tempB);
                    tempListColors.add(tempColor);
                    tempListQ.add(1);
                    System.out.println("ADDED!");
                    System.out.println(tempColor);
                }
                //System.out.println("R: " + tempR + "G: " + tempG + "B: " + tempB);
                
            }
        }
        
        int highest = 0, secondHighest = -1;
        
        int[][] tempListArray = new int[tempListQ.size()][2];
        
        for(int i=0; i<tempListQ.size(); i++){
            tempListArray[i][0] = tempListQ.get(i);
            tempListArray[i][1] = i;
        }
        
        Arrays.sort(tempListArray, (a, b) -> Double.compare(a[0], b[0]));
        
        /*for(int i=0; i<tempListQ.size(); i++){
            if(tempListQ.get(i)>=tempListQ.get(highest)){
                if(secondHighest!=-1){
                    secondHighest = highest;
                }
                
                highest=i;
            }else{
                if(secondHighest==-1){
                    secondHighest=i;
                }
            }
        }*/
        
        
        
        palette = new Color[2];
        
        palette[0] = tempListColors.get(tempListArray[tempListQ.size()-1][1]);
        palette[1] = tempListColors.get(tempListArray[tempListQ.size()-2][1]);
        
        System.out.println("Highest: " + tempListColors.get(tempListArray[tempListQ.size()-1][1]) + " Second Highest: " + tempListColors.get(tempListArray[tempListQ.size()-2][1]) + " tempListColors.size(): " + tempListColors.size());
        
        //palette[0] = tempListColors.get(highest);
        //palette[1] = tempListColors.get(secondHighest);
        
        //usesInlinePalette=true;
        
        
        System.out.println("numColors: " + tempListQ.size());
        
        //System.out.println("TR: " + tempListR.get(highest) + " TG: " + tempListG.get(highest) + " TB: " + tempListB.get(highest));
        //System.out.println("TR: " + tempListR.get(secondHighest) + " TG: " + tempListG.get(secondHighest) + " TB: " + tempListB.get(secondHighest));
        
        imageData = new boolean[tempWidth][tempHeight];
        
        int distanceTo0 = 0, distanceTo1 = 0;
        
        int tempR = 0;
        int tempG = 0;
        int tempB = 0;
        Color tempColor;
            
        for(int j=0; j<tempHeight; j++){
            for(int i=0; i<tempWidth; i++){
                tempColor = new Color(temp.getRGB(i, j));
                tempR = tempColor.getRed();
                tempG = tempColor.getGreen();
                tempB = tempColor.getBlue();
                
                distanceTo0 = Math.abs(tempR - palette[0].getRed()) + Math.abs(tempG - palette[0].getGreen()) + Math.abs(tempB - palette[0].getBlue());
                distanceTo1 = Math.abs(tempR - palette[1].getRed()) + Math.abs(tempG - palette[1].getGreen()) + Math.abs(tempB - palette[1].getBlue());
                
                //System.out.println(distanceTo0 + " " + distanceTo1);
                
                imageData[i][j] = distanceTo0 >= distanceTo1;
            }
        }
    }
    
}
