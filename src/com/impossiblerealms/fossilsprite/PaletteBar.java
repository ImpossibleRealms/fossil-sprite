/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import static com.impossiblerealms.fossilsprite.FossilSprite.screenSize;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JColorChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author gjgresham
 */
public class PaletteBar extends JPanel{
    
    FossilSprite imageEditor;
    
    public int scroll=0;
    public int maxScroll=0;
    
    public boolean draggingColor;
    public int dragColorX, dragColorY;
    public int dragColor=-1;
    
    public int dragDrawX=0, dragDrawY=0;
    
    public static int colorWidth = 16, colorHeight=16;
    
    public int numColumns = 4;
    
    public PaletteBar(FossilSprite imageEditor, int numColumns){
        this.numColumns=numColumns;
        this.imageEditor = imageEditor;
        setPreferredSize(new Dimension (colorWidth*numColumns, screenSize.height));
        setMaximumSize(new Dimension (colorWidth*numColumns, Integer.MAX_VALUE));
        System.out.println("maxScroll 1: " + maxScroll);
        maxScroll = ((imageEditor.palette.color.length * colorHeight) + colorHeight)/numColumns;
        
        PaletteInputManager input = new PaletteInputManager(this);
        addMouseListener(input);
        addMouseMotionListener(input);
        addMouseWheelListener(input);
    }
    
    @Override
    public void setPreferredSize(Dimension dimension){
        super.setPreferredSize(dimension);
        maxScroll = ((imageEditor.palette.color.length * colorHeight) + colorHeight)/numColumns;
        //maxScroll -= this.getHeight();
    }
    
    public void resize(){
        if(((imageEditor.palette.color.length * colorHeight) + colorHeight) > getHeight()){
            maxScroll = ((imageEditor.palette.color.length * colorHeight) + colorHeight)/numColumns;
        }else{
            maxScroll = getHeight();
        }
    }
    
    @Override
    public void paint(Graphics g) {
        if(imageEditor.paletteEnabled){
            paintActive(g);
        }else{
            paintInactive(g);
        }
    }
    
    public void paintActive(Graphics g){
        System.out.println(scroll + "   " + maxScroll);
        g.clearRect(0, 0, this.getWidth(), this.getHeight());
        g.setColor(Color.black);
        g.fillRect(0, -scroll, colorWidth * numColumns, (imageEditor.palette.color.length/numColumns) * colorHeight);
        for(int i=0; i<imageEditor.palette.color.length; i++){
            
            if(i/numColumns == imageEditor.palette.color.length/numColumns && i!=imageEditor.eraseColor && i!=imageEditor.activeColor){
                g.setColor(Color.black);
                g.fillRect(((i%numColumns)*colorWidth), (((i/numColumns)*colorHeight))-scroll, colorWidth, colorHeight);
            }
            
            if(i==imageEditor.eraseColor){
                g.setColor(Color.magenta);
                g.fillRect((i%numColumns)*colorWidth, ((i/numColumns)*colorHeight)-scroll, colorWidth, colorHeight);
            }
            
            if(i==imageEditor.activeColor){
                g.setColor(Color.yellow);
                g.fillRect((i%numColumns)*colorWidth, ((i/numColumns)*colorHeight)-scroll, colorWidth, colorHeight);
            }
            
            if(i==dragColor && draggingColor){
                //g.setColor(imageEditor.palette.color[i]);
                //g.fillRect(1, dragColorY-scroll, 24, 24);
            }else{
                g.setColor(imageEditor.palette.color[i]);
                g.fillRect(((i%numColumns)*colorWidth)+1, (((i/numColumns)*colorHeight)+1)-scroll, colorWidth-2, colorHeight-2);
            }
            
        }
        
        if(dragColor>=0 && draggingColor){
            g.setColor(imageEditor.palette.color[dragColor]);
            g.fillRect(dragColorX, dragColorY-scroll, colorWidth/2, colorHeight/2);
        }
    }
    
    public void paintInactive(Graphics g){
        g.clearRect(0, 0, this.getWidth(), this.getHeight());
    }
    
    public void addColor(){
        //JColorChooser colorChoose = new JColorChooser();
        
        Color temp = JColorChooser.showDialog(imageEditor.frame, "Add Color", Color.white);
        
        if(temp!=null){
            Color[] colors = new Color[imageEditor.palette.color.length+1];
        for(int i=0; i<imageEditor.palette.color.length; i++){
            colors[i] = imageEditor.palette.color[i];
        }
        colors[imageEditor.palette.color.length] = temp;
        imageEditor.palette.color = colors;
        resize();
        repaint();
        }
        
    }
    
    public void removeColor(){
        if(imageEditor.palette.color.length>2){
            Color[] colors = new Color[imageEditor.palette.color.length-1];
            for(int i=0; i<imageEditor.palette.color.length-1; i++){
                colors[i] = imageEditor.palette.color[i];
            }
            if(imageEditor.image.canRemoveColor()){
                imageEditor.palette.color = colors;
                resize();
                repaint();
            }else{
                JOptionPane.showMessageDialog(imageEditor.frame,
                "Cannot remove a palette color that is in use.",
                "Error",
                JOptionPane.ERROR_MESSAGE);
            }
        }else{
            JOptionPane.showMessageDialog(imageEditor.frame,
            "An image with less than two colors isn't much of an image, now is it?",
            "Error",
            JOptionPane.ERROR_MESSAGE);
        } 
    }
    
    public void reorderPalette(int oldID, int newID){
        
        if(newID >= imageEditor.palette.color.length){
            return;
        }
        
        Color temp = imageEditor.palette.color[oldID];
        int start = 0;
        if(oldID<newID){
            start=oldID;
            for(int i=start; i<imageEditor.palette.color.length; i++){
                if(i<newID){
                    imageEditor.palette.color[i]=imageEditor.palette.color[i+1];
                }else if(i==newID){
                    imageEditor.palette.color[i]=temp;
                }
            }
            for(int j=0; j<imageEditor.image.imageHeight; j++){
                for(int i=0; i<imageEditor.image.imageWidth; i++){
                    if(imageEditor.image.data[i][j]>oldID && imageEditor.image.data[i][j]<=newID){
                        imageEditor.image.data[i][j] = imageEditor.image.data[i][j]-1;
                    }else if(imageEditor.image.data[i][j]==oldID){
                        imageEditor.image.data[i][j]=newID;
                    }
                }
            }
        }else if(newID<oldID){
            Color temp2;
            int tempInt=oldID;
            int tempInt2;
            start=newID;
            for(int i=start; i<imageEditor.palette.color.length; i++){
                if(i>=newID && i<oldID){
                  temp2 = imageEditor.palette.color[i];
                  imageEditor.palette.color[i]=temp;
                  temp = temp2;
                }else if(i==oldID){
                    imageEditor.palette.color[i]=temp;
                }
            }
            for(int j=0; j<imageEditor.image.imageHeight; j++){
                for(int i=0; i<imageEditor.image.imageWidth; i++){
                    if(imageEditor.image.data[i][j]>=newID && imageEditor.image.data[i][j]<oldID){
                        imageEditor.image.data[i][j] = imageEditor.image.data[i][j]+1;
                    }else if(imageEditor.image.data[i][j]==oldID){
                        imageEditor.image.data[i][j]=newID;
                    }
                }
            }
        }
        
        imageEditor.activeColor=newID;
        
    }
    
}
