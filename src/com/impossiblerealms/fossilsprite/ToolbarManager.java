/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 *
 * @author gjgresham
 */
public class ToolbarManager implements ActionListener, ItemListener{
    
    public FossilSprite imageEditor;
    
    public ToolbarManager(FossilSprite imageEditor){
        this.imageEditor = imageEditor;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        switch(ae.getActionCommand()){
            case "New File":
                imageEditor.newImage();
                break;
                
            case "Open File":
                imageEditor.openImage();
                break;
                
            case "Save File":
                imageEditor.saveImage(imageEditor.currentFile);
                break;
                
            case "Save File As":
                imageEditor.saveImageAs();
                break;
                
            case "Resize Image":
                imageEditor.resizeImage();
                break;
                
            case "New Palette":
                imageEditor.newPalette();
                break;
            
            case "Open Palette":
                imageEditor.openPalette();
                break;
                
            case "Save Palette":
                imageEditor.savePalette();
                break;
                
            case "Edit Active Color":
                imageEditor.editActiveColor();
                break;
                
            case "Edit Background Color":
                imageEditor.editBackgroundColor();
                break;
                
            case "Import File":
                imageEditor.importFile();
                break;
                
            case "Export File":
                imageEditor.exportFile();
                break;
                
            case "Undo":
                imageEditor.undo();
                break;
                
            case "Redo":
                imageEditor.redo();
                break;
                
            case "About":
                imageEditor.about();
                break;
                
            default:
                imageEditor.notSupported();
                break;
        }
        
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
