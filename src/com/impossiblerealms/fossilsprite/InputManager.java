/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

/**
 *
 * @author gjgresham
 */

public class InputManager implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener{
    
    public int scrollSpeed = 4;
    
    public FossilSprite editor;
    public UndoRedoObject undo;
    
    public boolean resizing = false;
    public boolean resizingStage = false;
    public boolean isErase = false;
    public int newImageWidth = 0;
    public int newImageHeight = 0;
    
    public InputManager(FossilSprite editor){
        this.editor = editor;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                editor.scrollX -= scrollSpeed;
                editor.repaint();
                break;

            case KeyEvent.VK_RIGHT:
                editor.scrollX += scrollSpeed;
                editor.repaint();
                break;

            case KeyEvent.VK_UP:
                editor.scrollY -= scrollSpeed;
                editor.repaint();
                break;

            case KeyEvent.VK_DOWN:
                editor.scrollY += scrollSpeed;
                editor.repaint();
                break;
                    
            case KeyEvent.VK_S:
                
                //}
                break;

            default:
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        switch (ke.getKeyCode()){
                case KeyEvent.VK_LEFT:
                    
                    break;
                    
                default:
                    break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        if (editor.imageEditEnabled) {
            int mX = (me.getX() + editor.scrollX) / editor.zoom;
            int mY = (me.getY() + editor.scrollY) / editor.zoom;
            //System.out.println(mX);
            switch (me.getButton()) {
                case MouseEvent.BUTTON1:
                    switch (editor.cursorMode) {
                        case FossilSprite.PIXEL_MODE:
                            isErase = false;
                            //System.out.println("mX: " + mX);
                            //System.out.println("mY: " + mY);
                            if (mX >= 0 && mX < editor.image.imageWidth
                                    && mY >= 0 && mY < editor.image.imageHeight) {
                                IRImage imageTemp = new IRImage(editor, editor.image.revision, editor.image.imageWidth, editor.image.imageHeight);
                                imageTemp.setData(editor.image.data);
                                editor.undoStack.push(imageTemp);
                                editor.redoStack.clear();
                                editor.undo.setEnabled(true);
                                editor.redo.setEnabled(false);
                                //System.out.println("IMAGE DATA AT 0, 0: " + editor.undoStack.peek().data[0][0]);
                                editor.image.data[mX][mY] = editor.activeColor;
                                if (editor.fileCanChange = true) {
                                    editor.fileChanged = true;
                                }
                                editor.repaint();
                                //System.out.println("IMAGE DATA AT 0, 0: " + editor.undoStack.peek().data[0][0]);
                            }
                            break;
                    }
                    
                    break;

                case MouseEvent.BUTTON3:
                    isErase = true;
                    //System.out.println("mX: " + mX);
                    //System.out.println("mY: " + mY);
                    if (mX >= 0 && mX < editor.image.imageWidth
                            && mY >= 0 && mY < editor.image.imageHeight) {
                        editor.image.data[mX][mY] = editor.eraseColor;
                        if (editor.fileCanChange = true) {
                            editor.fileChanged = true;
                        }
                        editor.repaint();
                    }
                    break;
            }
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        
        //editor.repaint();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {
        if((int)(editor.zoom+mwe.getWheelRotation())>=1){
            editor.zoom += (int)mwe.getWheelRotation();
            editor.repaint();
        }
    }

    @Override
    public void mouseDragged(MouseEvent me) {
        
        int mX = (me.getX()+editor.scrollX)/editor.zoom;
        int mY = (me.getY()+editor.scrollY)/editor.zoom;
        
        //System.out.println("Dragged " + mX + " " + mY + " " + me.getButton());
        //System.out.println(mX);
        if(!isErase){
            //System.out.println("mX: " + mX);
            //System.out.println("mY: " + mY);
            if(mX >= 0 && mX < editor.image.imageWidth &&
                    mY >= 0 && mY < editor.image.imageHeight){
                editor.image.data[mX][mY] = editor.activeColor;
                editor.repaint();
            }
        }else{
            //System.out.println("mX: " + mX);
            //System.out.println("mY: " + mY);
            if(mX >= 0 && mX < editor.image.imageWidth &&
                    mY >= 0 && mY < editor.image.imageHeight){
                editor.image.data[mX][mY] = editor.eraseColor;
                //editor.repaint();
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        int mX = (me.getX()+editor.scrollX)/editor.zoom;
        int mY = (me.getY()+editor.scrollY)/editor.zoom;
        //System.out.println(mX);
            //System.out.println("mX: " + mX);
            //System.out.println("mY: " + mY);
            //if(mX >= 0 && mX < editor.image.imageWidth &&
            //        mY >= 0 && mY < editor.image.imageHeight){
                //System.out.println(editor.image.data[mX][mY]);
            //}
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
