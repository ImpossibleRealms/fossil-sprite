/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import com.impossiblerealms.fossilsprite.filters.IRImageFilter;
import com.impossiblerealms.fossilsprite.filters.OBGFilter;
import com.impossiblerealms.fossilsprite.filters.PNGFilter;
import com.impossiblerealms.fossilsprite.filters.PaletteFilter;
import com.impossiblerealms.fossilsprite.filters.SPALFilter;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
//import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author gjgresham
 */
public class FossilSprite extends JPanel{
    
    public static Dimension screenSize = new Dimension(800,600);
    public static String programName = "Fossil Sprite";
    
    public static final int PIXEL_MODE=0;
    
    //Menu
    MenuBar menuBar;
    Menu file, edit, help;
    MenuItem newFile, openFile, saveFile, saveFileAs;
    MenuItem importFile, exportFile;
    MenuItem resizeImage, newPalette, openPalette, savePalette, editActiveColor, editBackgroundColor;
    MenuItem undo, redo;
    MenuItem about;
    
    public BufferedImage screen;
    public BufferedImage graphic;
    public BufferedImage debug;
    
    public Graphics2D OBG;
    
    public JFileChooser chooser;
    
    public InputManager input;
    
    //public OBGFile obgFile;
    public IRImage image;
    
    public PaletteFile palette;
    int activeColor = 0, eraseColor = 0;
    int cursorMode=0;
    public Color backgroundColor = Color.gray;
    
    public int scrollX=-300, scrollY=-200, zoom=8;
    
    public Frame frame;
    PaletteBar paletteBar;
    
    public ToolbarManager toolbarManager;
    
    public File currentFile;
    
    public boolean paletteEnabled=false;
    public boolean imageEditEnabled=false;
    
    public boolean fileChanged=false, fileCanChange=false, fileReorganizePalette=true;
    
    //ArrayDeque<UndoRedoObject> undoStack;
    //ArrayDeque<UndoRedoObject> redoStack;
    
    ArrayDeque<IRImage> undoStack;
    ArrayDeque<IRImage> redoStack;
    
    public int numColumns=1;
    
    //public JPanel imagePanel;
    
    public FossilSprite(String programName){
        //super(programName);
        
        loadINI();
        
        undoStack = new ArrayDeque<>();
        redoStack = new ArrayDeque<>();
        
        frame = new Frame(programName);
        
        System.out.println("Max Short: " + (int)(Character.MAX_VALUE));
        
        //imagePanel = new JPanel();
        //imagePanel.setPreferredSize(screenSize);
        setPreferredSize(screenSize);
        
        
        
        //Create the menu bar.
        
        toolbarManager = new ToolbarManager(this);
        
        //Build the first menu.
        file = new Menu("File");
        newFile = new MenuItem("New File");
        newFile.setActionCommand("New File");
        newFile.addActionListener(toolbarManager);
        file.add(newFile);
        
        openFile = new MenuItem("Open File");
        openFile.setActionCommand("Open File");
        openFile.addActionListener(toolbarManager);
        file.add(openFile);
        
        saveFile = new MenuItem("Save");
        saveFile.setActionCommand("Save File");
        saveFile.addActionListener(toolbarManager);
        saveFile.setEnabled(false);
        file.add(saveFile);
        
        saveFileAs = new MenuItem("Save As");
        saveFileAs.setActionCommand("Save File As");
        saveFileAs.addActionListener(toolbarManager);
        saveFileAs.setEnabled(false);
        file.add(saveFileAs);
        
        file.addSeparator();
        
        importFile = new MenuItem("Import File");
        importFile.setActionCommand("Import File");
        importFile.addActionListener(toolbarManager);
        //importFile.setEnabled(false);
        file.add(importFile);
        
        exportFile = new MenuItem("Export File");
        exportFile.setActionCommand("Export File");
        exportFile.addActionListener(toolbarManager);
        exportFile.setEnabled(false);
        file.add(exportFile);
        
        edit = new Menu("Edit");
        resizeImage = new MenuItem("Resize Image");
        resizeImage.setActionCommand("Resize Image");
        resizeImage.addActionListener(toolbarManager);
        resizeImage.setEnabled(false);
        edit.add(resizeImage);
        
        edit.addSeparator();
        
        newPalette = new MenuItem("New Palette");
        newPalette.setActionCommand("New Palette");
        newPalette.addActionListener(toolbarManager);
        newPalette.setEnabled(false);
        edit.add(newPalette);
        
        openPalette = new MenuItem("Open Palette");
        openPalette.setActionCommand("Open Palette");
        openPalette.addActionListener(toolbarManager);
        openPalette.setEnabled(false);
        edit.add(openPalette);
        
        savePalette = new MenuItem("Save Palette");
        savePalette.setActionCommand("Save Palette");
        savePalette.addActionListener(toolbarManager);
        savePalette.setEnabled(false);
        edit.add(savePalette);
        
        editActiveColor = new MenuItem("Edit Active Color");
        editActiveColor.setActionCommand("Edit Active Color");
        editActiveColor.addActionListener(toolbarManager);
        editActiveColor.setEnabled(false);
        edit.add(editActiveColor);
        
        editBackgroundColor = new MenuItem("Edit Background Color");
        editBackgroundColor.setActionCommand("Edit Background Color");
        editBackgroundColor.addActionListener(toolbarManager);
        //editBackgroundColor.setEnabled(false);
        edit.add(editBackgroundColor);
        
        edit.addSeparator();
        
        undo = new MenuItem("Undo");
        undo.setActionCommand("Undo");
        undo.addActionListener(toolbarManager);
        undo.setEnabled(false);
        edit.add(undo);
        
        redo = new MenuItem("Redo");
        redo.setActionCommand("Redo");
        redo.addActionListener(toolbarManager);
        redo.setEnabled(false);
        edit.add(redo);
        
        help = new Menu("Help");
        about = new MenuItem("About");
        about.setActionCommand("About");
        about.addActionListener(toolbarManager);
        help.add(about);
        
        //file.setMnemonic(KeyEvent.VK_A);
        //file.getAccessibleContext().setAccessibleDescription(
        //"The only menu in this program that has menu items");
        menuBar = new MenuBar();
        frame.setMenuBar(menuBar);
        menuBar.add(file);
        menuBar.add(edit);
        menuBar.add(help);
        
        
        
        //edit = new JMenu("Edit");
        //menuBar.add(edit);
        
        //help = new JMenu("Help");
        //menuBar.add(help);
        
        //this.setJMenuBar(menuBar);

        frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                if(fileChanged){
                    saveOnQuit();
                }
                frame.dispose();
                System.exit(0);
            }
        }
        );
        
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        input = new InputManager(this);
        
        frame.addKeyListener(input);
        addMouseListener(input);
        addMouseMotionListener(input);
        addMouseWheelListener(input);
        
        //palette = new PaletteFile(new File("GravityPalette.spal"));
        paletteBar = new PaletteBar(this, numColumns);
        
        //openImage();
        
        JPanel container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.X_AXIS));
        //setOpaque(false);
        container.add(this);
        container.add(paletteBar);
        frame.add(container);
        
        frame.pack();
        frame.setFocusable(true);
        frame.setLocationRelativeTo(null);
        
        //validate();
        frame.setVisible(true);
        
        //repaint();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FossilSprite frame = new FossilSprite(programName);
    }
    
    public void loadINI(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("fossilsprite.ini"));
            String temp, newValue = "";
            String varName = "";
            boolean ignoreInput = false, ignoreUntilLineEnd = false;
            while ((temp = reader.readLine()) != null) {
                for(int i=0; i<temp.length(); i++){
                    switch(temp.charAt(i)){
                        case 59: // the character is a ;
                            ignoreUntilLineEnd=true;
                            break;
                        
                        case 61: // the character is a =
                            varName = newValue;
                            newValue = "";
                            break;
                            
                        case 91: // the character is a [
                            ignoreInput=true;
                            break;
                            
                        case 93: // the character is a ]
                            ignoreInput = false;
                            break;
                            
                        default:
                            if(!ignoreInput && !ignoreUntilLineEnd){
                                newValue = newValue+Character.toString(temp.charAt(i));
                            }
                            break;
                    }
                }
                ignoreUntilLineEnd=false;
                switch(varName){
                    case "windowWidth":
                        screenSize.setSize(Integer.parseInt(newValue), screenSize.getHeight());
                        break;
                        
                    case "windowHeight":
                        screenSize.setSize(screenSize.getWidth(), Integer.parseInt(newValue));
                        break;
                        
                    case "paletteCol":
                        numColumns = Integer.parseInt(newValue);
                        break;
                        
                    case "defaultPalette":
                        System.out.println("Parsed line: " + varName + "=" + newValue);
                        loadPalette(newValue);
                        break;
                        
                    default:
                        System.out.println("Unparsed line: " + varName + "=" + newValue);
                        break;
                }
                newValue="";
                varName="";
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FossilSprite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FossilSprite.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @Override
    public void paint(Graphics g) {
     
     //Here is how we used to draw a square with width
     //of 200, height of 200, and starting at x=50, y=50.
     
      super.paintComponent(g);
    Graphics2D g2d = (Graphics2D)g;
    
     g2d.setColor(backgroundColor);
     g2d.fillRect(0, 0, getWidth(), getHeight());
     
     //OBG.clearRect(0, 0, obgFile.imageWidth, obgFile.imageHeight);
     //OBG.setColor(new Color(255, 255, 255, 0));
     //OBG.fillRect(0, 0, obgFile.imageWidth, obgFile.imageHeight);
     
    //clear
    
     
     if(image != null){
        OBG.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR));
        OBG.fillRect(0, 0, image.imageWidth, image.imageHeight);

        //reset composite
        OBG.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));
         
        System.out.println("Pal Size: "+palette.color.length);
        for(int j=0; j<image.imageHeight; j++){
            for(int i=0; i<image.imageWidth; i++){
            
                if(image.data[i][j] < palette.color.length){
                    OBG.setColor(palette.color[image.data[i][j]]);
                }else{
                    OBG.setColor(Color.MAGENTA);
                }
                //OBG.
                //graphic.setRGB(palette.color[image.data[i][j]].getRGB(), i, j);
                graphic.setRGB(i, j, OBG.getColor().getRGB());
                //OBG.fillRect(i, j, 1, 1);
                
            /*if(image.data[i][j]){
                if(obgFile.usesInlinePalette){
                    OBG.setColor(obgFile.palette[1]);
                }else{
                    OBG.setColor(palette.color[1]);
                }
                OBG.fillRect(i, j, 1, 1);
            }else{
                if(obgFile.usesInlinePalette){
                    OBG.setColor(obgFile.palette[0]);
                }else{
                    OBG.setColor(palette.color[0]);
                }
                OBG.fillRect(i, j, 1, 1);
            }*/
            }
        }
     
     g2d.drawImage(graphic, 0-scrollX, 0-scrollY, (image.imageWidth*zoom)-scrollX, (image.imageHeight*zoom)-scrollY, 0, 0, image.imageWidth, image.imageHeight, null);
     
     if(zoom>=4){
         g2d.setColor(Color.black);
         for(int j=0; j<image.imageHeight; j++){
            for(int i=0; i<image.imageWidth; i++){
                g2d.drawRect((i*zoom)-scrollX, (j*zoom)-scrollY, zoom, zoom);
            }
         }
     }
     
     //g2d.drawImage(debug, 0-scrollX, 0-scrollY, (image.imageWidth*zoom)-scrollX, (image.imageHeight*zoom)-scrollY, 0, 0, image.imageWidth, image.imageHeight, null);
     }
     
   }
    
    public void loadOBG(File graphicImage){
        
    }
    
    public void resizeImageCanvas(){
        graphic = new BufferedImage(image.imageWidth, image.imageHeight, BufferedImage.TYPE_INT_ARGB);
        OBG = graphic.createGraphics(); 
    }
    
    public void newImage(){
        FileCreatorDialog fcg = new FileCreatorDialog(frame);
        //fcg.setVisible(true);
        String results[] = fcg.run();
        if(results[0]!=null){
            System.out.println(results[0]);
            //image = IRImage.loadOBG(currentFile)
            image = new IRImage(this, 0, Integer.parseInt(results[0]), Integer.parseInt(results[1]));
            //obgFile = new OBGFile(0, Integer.parseInt(results[0]), Integer.parseInt(results[1]));
            graphic = new BufferedImage(image.imageWidth, image.imageHeight, BufferedImage.TYPE_INT_ARGB);
            OBG = graphic.createGraphics();
            
            enableImageOptions();
        }
    }
    
    public void openImage(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new OBGFilter());
        fileChooser.setFileFilter(new IRImageFilter());
            if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                currentFile = fileChooser.getSelectedFile();
                System.out.println("Current File Path: " + currentFile.getPath());
        
                if(currentFile.getAbsolutePath().endsWith(".obg")){
                    image = IRImage.loadOBG(this, currentFile);
                }
                
                if(currentFile.getAbsolutePath().endsWith(".tbg")){
                    image = IRImage.loadTBG(this, currentFile);
                }
                
                if(currentFile.getAbsolutePath().endsWith(".fbg")){
                    image = IRImage.loadFBG(this, currentFile);
                }
                
                if(currentFile.getAbsolutePath().endsWith(".ebg")){
                    image = IRImage.loadEBG(this, currentFile);
                }
                
                if(currentFile.getAbsolutePath().endsWith(".sbg")){
                    image = IRImage.loadSBG(this, currentFile);
                }
                
                File tempPal = new File(currentFile.getAbsolutePath().replaceFirst("[.][^.]+$", "") + ".spal");
                
                if(tempPal.exists()){
                    palette = new PaletteFile(tempPal);
                    paletteBar.resize();
                }
                
                //obgFile = new OBGFile(currentFile);
        
                graphic = new BufferedImage(image.imageWidth, image.imageHeight, BufferedImage.TYPE_INT_ARGB);
        
                OBG = graphic.createGraphics();
                enableImageOptions();
            }
    }
    
    public void saveImageAs(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new IRImageFilter());
            if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                currentFile = file;
                
                FileSaveOptions fso = new FileSaveOptions(frame, fileReorganizePalette);
        //fcg.setVisible(true);
            String results[] = fso.run();
        if(results[0]!=null){
            if(results[0].equals("true")){
                fileReorganizePalette=true;
            }else{
                fileReorganizePalette=false;
            }
        }
                // save to file
                saveImage(file);
            }
    }
    
    public void saveImage(File file){
        fileChanged=false;
        image.save(file, this, fileReorganizePalette);
    }
    
    public void resizeImage(){
        //input.resizing = true;
        //repaint();
        FileResizeDialog fcg = new FileResizeDialog(frame);
        //fcg.setVisible(true);
        String results[] = fcg.run();
         if(results[0]!=null){
            image.resize(Integer.parseInt(results[0]), Integer.parseInt(results[1]), 0);
            graphic = new BufferedImage(image.imageWidth, image.imageHeight, BufferedImage.TYPE_INT_ARGB);
            OBG = graphic.createGraphics();
            repaint();
        }
    }
    
    public void importFile(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new PNGFilter());
            if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                currentFile = fileChooser.getSelectedFile();
        
                //obgFile = new OBGFile(currentFile);
                //image = IRImage.loadOBG(this, currentFile);
                image = IRImage.importImage(this, currentFile);
                
                //palette.color = obgFile.palette;
        
                graphic = new BufferedImage(image.imageWidth, image.imageHeight, BufferedImage.TYPE_INT_ARGB);
        
                OBG = graphic.createGraphics();
                enableImageOptions();
            }
    }
    
    public void undo(){
        IRImage imageTemp = new IRImage(this, image.revision, image.imageWidth, image.imageHeight);
        imageTemp.setData(image.data);
        System.out.println(imageTemp.data[0][0]);
        redoStack.push(imageTemp);
        redo.setEnabled(true);
        IRImage imageTemp2 = new IRImage(this, image.revision, image.imageWidth, image.imageHeight);
        imageTemp2.setData(undoStack.pop().data);
        System.out.println(imageTemp2.data[0][0]);
        //undoStack.peek()
        image.setData(imageTemp2.data);
        repaint();
        System.out.println("Undone. " + imageTemp2.data[0][0] + " " + imageTemp.data[0][0] + " " + image.data[0][0]);
        if(undoStack.isEmpty()){
            undo.setEnabled(false);
        }
    }
    
    public void redo(){
        IRImage imageTemp = new IRImage(this, image.revision, image.imageWidth, image.imageHeight);
        imageTemp.data = image.data;
        undoStack.push(imageTemp);
        undo.setEnabled(true);
        image.data = redoStack.pop().data;
        repaint();
        if(redoStack.isEmpty()){
            redo.setEnabled(false);
        }
    }
    
    public void exportFile(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new PNGFilter());
            if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                file = new File(file.getAbsolutePath().replaceFirst("[.][^.]+$", "") + ".png");
                try{
                    ImageIO.write(graphic, "png", file);
                }catch(Exception e){
                    
                }
                
            }
    }
    
    public void about(){
        JOptionPane.showMessageDialog(frame,
            programName + " v0.2 alpha.\n"
                    + "Current build supports editing of the following custom formats: \n\n"
                    + ".OBG   .TBG   .FBG   .EBG   .SBG\n\n"
                    + "Current build has been tested on importing the following standard image formats: \n\n"
                    + ".PNG   .JPG\n\n"
                    + "However, it should work with anything that is recognized as an image format in Java.\n"
                    + "Initial public source release, this program is licensed under the GPL license. \n\n"
                    + "Have fun, \n"
                    + "Lucina",
            "About",
            JOptionPane.PLAIN_MESSAGE);
    }
    
    public void enableImageOptions(){
        fileCanChange=true;
        saveFile.setEnabled(true);
            saveFileAs.setEnabled(true);
            resizeImage.setEnabled(true);
            exportFile.setEnabled(true);
            newPalette.setEnabled(true);
            openPalette.setEnabled(true);
            savePalette.setEnabled(true);
            editActiveColor.setEnabled(true);
            //undo.setEnabled(true);
            //redo.setEnabled(true);
            
            paletteEnabled=true;
            paletteBar.repaint();
            
            imageEditEnabled=true;
            repaint();
    }
    
    public void newPalette(){
        int highestColor=-1;
        for(int j=0; j<image.imageHeight; j++){
            for(int i=0; i<image.imageWidth; i++){
                if((image.data[i][j] + 1) > highestColor){
                    highestColor = image.data[i][j] + 1;
                }
            }
        }
        
        if(highestColor<2){
            highestColor = 2;
        }
        palette = new PaletteFile(highestColor);
        paletteBar.resize();
        repaint();
        paletteBar.repaint();
    }
    
    public void openPalette(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new PaletteFilter());
        fileChooser.setFileFilter(new SPALFilter());
            if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                currentFile = fileChooser.getSelectedFile();
                System.out.println("Current File Path: " + currentFile.getPath());
        
                if(currentFile.getAbsolutePath().endsWith(".spal")){
                    //File tempPal = new File(currentFile.getAbsolutePath());
                
                    if(currentFile.exists()){
                        loadPalette(currentFile);
                    } 
                }
            }
        repaint();
        paletteBar.repaint();
    }
    
    public void loadPalette(File paletteFile){
        palette = new PaletteFile(paletteFile);
        if(paletteBar!=null){
            paletteBar.resize();
        }
    }
    
    public void loadPalette(String paletteFile){
        File file = new File(paletteFile);
        loadPalette(file);
    }
    
    public void savePalette(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new SPALFilter());
            if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                currentFile = file;
                // save to file
                try{
        DataOutputStream os = null;
        
        os = new DataOutputStream(new FileOutputStream(file + ".spal"));
        
        char numColors = (char)(palette.color.length & 0xFFFF);
        
        os.writeChar(numColors);
        
        for(int i=0; i<numColors; i++){
            os.writeByte(palette.color[i].getRed());
            os.writeByte(palette.color[i].getGreen());
            os.writeByte(palette.color[i].getBlue());
            os.writeByte(palette.color[i].getAlpha());
        }
        
        os.close();
        }catch(Exception e){
            
        }
            }
        
    }
    
    public void editActiveColor(){
        //System.out.println("Editing Palette");
        JColorChooser colorChoose = new JColorChooser();
        JColorChooser.createDialog(frame, "Edit Color",
            true, colorChoose, null,
            null).setVisible(true);
        palette.color[activeColor]=colorChoose.getColor();
        repaint();
        paletteBar.repaint();
    }
    
    public void editBackgroundColor(){
        //System.out.println("Editing Palette");
        JColorChooser colorChoose = new JColorChooser();
        JColorChooser.createDialog(frame, "Edit Color",
            true, colorChoose, null,
            null).setVisible(true);
        backgroundColor=colorChoose.getColor();
        repaint();
        paletteBar.repaint();
    }
    
    public void notSupported(){
        //custom title, error icon
        JOptionPane.showMessageDialog(frame,
            "Operation not supported yet.",
            "Error",
            JOptionPane.ERROR_MESSAGE);
    }
    
    public void saveOnQuit(){
        //notSupported();
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog (null, "This file has been changed. Would you like to save changes to this file before quitting?","Warning",dialogButton);
        if(dialogResult == JOptionPane.YES_OPTION){
            if(currentFile.exists()){
                saveImage(currentFile);
            }else{
                saveImageAs();
            }
        }
    }
        // TODO code application logic here  
}
