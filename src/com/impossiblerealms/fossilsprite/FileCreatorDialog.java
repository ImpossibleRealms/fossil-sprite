/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author gjgresham
 */
public class FileCreatorDialog extends JDialog implements ActionListener{
    
    public String[] data;
    public int maxSize;
    
    private JButton btnOk;
    private JButton btnCancel;
    private JSpinner widthSpinner, heightSpinner;
    
    public FileCreatorDialog(Frame parent){
        super(parent, "Create File", true);
        this.setResizable(false);
        Point loc = parent.getLocation();
        setLocation(loc.x+80,loc.y+80);
        data = new String[2]; // set to amount of data items
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipadx=5;
        c.ipady=5;
        
        // Set up the formatting for the file size spinners
        SpinnerModel sizeModel =
        new SpinnerNumberModel(16, //initial value
                               1, //min
                               65535, //max
                               1);  //step
        
        SpinnerModel sizeModel2 =
        new SpinnerNumberModel(16, //initial value
                               1, //min
                               65535, //max
                               1);  //step
        
        // add components to the dialog
        c.gridx=0;
        c.gridy=0;
        JLabel widthLabel = new JLabel("Image Width:");
        panel.add(widthLabel, c);
        c.gridx=1;
        //c.gridy=0;
        widthSpinner = new JSpinner(sizeModel);
        panel.add(widthSpinner,c);
        c.gridx=0;
        c.gridy=1;
        JLabel heightLabel = new JLabel("Image Height:");
        panel.add(heightLabel, c);
        c.gridx=1;
        //c.gridy=1;
        heightSpinner = new JSpinner(sizeModel2);
        panel.add(heightSpinner, c);
        
      c.gridx=0;
      c.gridy=2;  
      btnOk = new JButton("Ok");
      btnOk.addActionListener(this);
      
      panel.add(btnOk, c);
      c.gridx=1;
      //c.gridy=0;
      btnCancel = new JButton("Cancel");
      btnCancel.addActionListener(this);
      
      panel.add(btnCancel, c);
      getContentPane().add(panel);
      pack();
        
    }
    
    public void actionPerformed(ActionEvent ae) {
      Object source = ae.getSource();
      if (source == btnOk) {
         //data[0] = widthSpinner.getText();
         data[0] = widthSpinner.getValue().toString();
         data[1] = heightSpinner.getValue().toString();
      }
      else {
         data[0] = null;
      }
      dispose();
   }
    
    public String[] run() {
      this.setVisible(true);
      return data;
   }
    
}
