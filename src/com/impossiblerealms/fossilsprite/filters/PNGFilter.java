/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite.filters;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author gjgresham
 */
public class PNGFilter extends FileFilter{

    @Override
    public boolean accept(File file) {
        if(file.getName().endsWith("png")||file.isDirectory()){
                return true;
            }
        /*String temp = file.getName();
        String[] temp2 = temp.split(".");
        System.out.println(temp2.length);
        if(temp2.length>0){
            if(temp2[temp2.length-1].equalsIgnoreCase("obg")){
                return true;
            }
        }*/
        return false;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDescription() {
        return "PNG Image Files (.png)";
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
