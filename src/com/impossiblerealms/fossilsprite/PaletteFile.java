/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gjgresham
 */
public class PaletteFile {
    
    public Color[] color;
    
    public PaletteFile(){
        //Color[] defaultColors = {new Color(0, 0, 0), new Color (255, 255, 255)};
        this(new Color[] {new Color (255, 255, 255), new Color(0, 0, 0)});
    }
    
    public PaletteFile(int numColors){
        color = new Color[numColors];
        for(int i=0; i<numColors; i++){
            color[i] = new Color (255, 255, 255);
        }
    }
    
    public PaletteFile(Color[] colors){
        //int numColors = colors.length;
        color = colors;
    }
    
    public PaletteFile(File file){
        int numColors = 0;
        byte[] paletteBytes = new byte[(int)file.length()];
        try {
            FileInputStream in=new FileInputStream(file);
            in.read(paletteBytes);
            in.close();
            
            numColors = DataTools.convertByte(paletteBytes[0]);
            numColors = numColors << 8;
            numColors = numColors + DataTools.convertByte(paletteBytes[1]);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FossilSprite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FossilSprite.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        color = new Color[numColors];
        
        int offset = 0;
        if(paletteBytes!=null){
            for(int i=0; i<numColors; i++){
                color[i] = new Color(DataTools.convertByte(paletteBytes[2+offset]), DataTools.convertByte(paletteBytes[2+offset+1]), DataTools.convertByte(paletteBytes[2+offset+2]), DataTools.convertByte(paletteBytes[2+offset+3]));
                //System.out.println("Color " + (i+1) + ":" + color[i]);
                offset+=4;
            }
        }
    }
    
}
