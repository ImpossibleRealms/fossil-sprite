/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 *
 * @author gjgresham
 */
public class PaletteInputManager implements MouseListener, MouseMotionListener, MouseWheelListener{
    
    public PaletteBar palette;
    public boolean mouseDown;
    public boolean draggingColor;
    
    public PaletteInputManager(PaletteBar palette){
        this.palette = palette;
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        
        int mX = me.getX();
        int mY = (me.getY()+palette.scroll);
        //System.out.println(mX);
        switch (me.getButton()){
            case MouseEvent.BUTTON1:
                System.out.println("mX: " + mX);
                System.out.println("mY: " + mY);
                if(mY/palette.colorHeight<Math.ceil((palette.imageEditor.palette.color.length+(palette.numColumns-1))/palette.numColumns)){
                    if((mY/palette.colorHeight)*palette.numColumns + (mX/palette.colorWidth) < palette.imageEditor.palette.color.length){
                        palette.imageEditor.activeColor = (mY/palette.colorHeight)*palette.numColumns + (mX/palette.colorWidth);
                        palette.dragColor = (mY/palette.colorHeight)*palette.numColumns + (mX/palette.colorWidth);
                    }
                    System.out.println("Drag Color: " + palette.dragColor);
                    
                    mouseDown=true;
                    System.out.println("Active Color: " + palette.imageEditor.activeColor);
                }else{
                    palette.addColor();
                }
                palette.repaint();
                break;
                
            case MouseEvent.BUTTON3:
                //System.out.println("mX: " + mX);
                //System.out.println("mY: " + mY);
                if(mY/palette.colorHeight<Math.ceil((palette.imageEditor.palette.color.length+(palette.numColumns-1))/palette.numColumns)){
                    palette.imageEditor.eraseColor = mY/palette.colorHeight;
                    //System.out.println("Active Color: " + palette.imageEditor.activeColor);
                }else{
                    palette.removeColor();
                }
                palette.repaint();
                break;
        }
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        mouseDown = false;
        if(palette.draggingColor){
            palette.draggingColor=false;
            palette.reorderPalette(palette.dragColor, (palette.dragColorY/palette.colorHeight)*palette.numColumns + (palette.dragColorX/palette.colorWidth));
            palette.dragColor = -1;
            palette.repaint();
            palette.imageEditor.repaint();
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {
        
        palette.scroll += (int)mwe.getWheelRotation() * 10;
        
        if(palette.scroll<0){
            palette.scroll=0;
        }
        if(palette.scroll>palette.maxScroll - palette.getHeight()){
            if(palette.maxScroll<palette.getHeight()){
                palette.scroll=0;
            }else{
                palette.scroll=palette.maxScroll - palette.getHeight();
            }
        }
        //System.out.println(palette.scroll + " " + (palette.maxScroll - palette.getHeight()));
        palette.repaint();
    }

    @Override
    public void mouseDragged(MouseEvent me) {
        System.out.println(MouseEvent.BUTTON1);
        if(mouseDown){
            int mX = me.getX();
            int mY = (me.getY()+palette.scroll);
            System.out.println("mX: " + mX);
            System.out.println("mY: " + mY);
            if(mY/palette.colorHeight<palette.imageEditor.palette.color.length){
                //palette.draggingColor=true;
                palette.dragColorX = mX;
                palette.dragColorY = mY;
                palette.draggingColor=true;
                //System.out.println("Active Color: " + palette.imageEditor.activeColor);
            }else{
                palette.addColor();
            }
            palette.repaint();
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
