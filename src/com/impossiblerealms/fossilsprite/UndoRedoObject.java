/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impossiblerealms.fossilsprite;

import java.util.ArrayList;

/**
 *
 * @author gjgresham
 */
public class UndoRedoObject {
    
    public ArrayList<Integer> pixelValues;
    public ArrayList<Integer> pixelLocationsX;
    public ArrayList<Integer> pixelLocationsY;
    
    public UndoRedoObject(){
        pixelValues = new ArrayList<>();
        pixelLocationsX = new ArrayList<>();
        pixelLocationsY = new ArrayList<>();
    }
    
    public void alter(int x, int y, int value){
        boolean exists=false;
        for(int i=0; i<pixelValues.size(); i++){
            if(pixelLocationsX.get(i)==x && pixelLocationsY.get(i)==y){
                exists=true;
                pixelValues.set(i, value);
            }
        }
        if(!exists){
            pixelValues.add(value);
            pixelLocationsX.add(x);
            pixelLocationsY.add(y);
        }
    }
    
    public void apply(IRImage image){
        for(int i=0; i<pixelValues.size(); i++){
            image.data[pixelLocationsX.get(i)][pixelLocationsY.get(i)] = pixelValues.get(i);
        }
    }
    
}
